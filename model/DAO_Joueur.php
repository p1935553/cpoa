<?php

require_once('variables.php');

class DAO_Joueur {
    private $bdd;

    public function __construct() {
        try {
            $this->bdd = new PDO("mysql:dbname=".$GLOBALS['dbName'].";host=localhost", $GLOBALS['dbName'], $GLOBALS['dbMdp']);
        }
        catch (PDOException $e) {
            $this->bdd = null;
            die($e->getMessage());
        }
    }

    public function findAll() {
        $line = null;
        $res = null;
        $sql = "select * from joueur";
        $query = $this->bdd->prepare($sql);
        $query->execute();
        while($line = $query->fetch()) {
            $res[] = new Joueur($line['idJoueur'],$line['nom'],$line['prenom'],$line['age'],$line['nationalite']);
        }
        $query->closecursor();
        return $res;
    }

    public function findByID($id) {
        $sql = "select * from joueur where idJoueur = ?";
        $query = $this->bdd->prepare($sql);
        $query->execute(array($id));
        if ($line = $query->fetch()) {
            return new Joueur($line['idJoueur'],$line['nom'],$line['prenom'],$line['age'],$line['nationalite']);
        } else {
            return null;
        }
        $query->closecursor();
    }

    public function findLike($str) {
        $line = null;
        $res = null;
        $like = '%' . $str . '%';
        $sql = "select * from joueur where nom like ? or prenom like ?";
        $query = $this->bdd->prepare($sql);
        $query->execute(array($like, $like));
        while($line = $query->fetch()) {
            $res[] = new Joueur($line['idJoueur'],$line['nom'],$line['prenom'],$line['age'],$line['nationalite']);
        }
        $query->closecursor();
        return $res;
    }

    public function deleteById($id)
    {
        $query = $this->bdd->prepare('delete from joueur where idJoueur = :id');
        $query->execute(array('id' => $id));
    }

    public function updatePlayerById($id, $nom, $prenom, $age, $nationalite)
    {
        $query = $this->bdd->prepare('update joueur set nom = :nom, prenom = :prenom, age = :age, nationalite = :nationalite where idJoueur = :id');
        $query->execute(array('id' => $id, 'nom' => $nom, 'prenom' => $prenom, 'age' => $age, 'nationalite' => $nationalite));
    }

    public function addPlayer($nom, $prenom, $age, $nationalite)
    {
        $query = $this->bdd->prepare('insert into joueur values(DEFAULT, ?, ?, ?, ?)');
        $query->execute(array($nom, $prenom, $age, $nationalite));
    }

    public function countVictories($id){
        $query = $this->bdd->prepare('select count(idMatch) as nb from matchs where idGagnant = ? ');
        $query->execute(array($id));
        $line = $query->fetch();
        return $line['nb'];
    }

    public function countDefeats($id){
        $query = $this->bdd->prepare('select count(idMatch) as nb from matchs where idJoueur1 = ? or idJoueur2 = ? ');
        $query->execute(array($id,$id));
        $line = $query->fetch();
        return $line['nb'] - $this->countVictories($id);
    }

    public function findByTournamentAndRoundId($idTournament, $numTour){
      $res = null;
      $sql = "SELECT * FROM joueur WHERE idJoueur IN (SELECT idJoueur1 FROM matchs WHERE idtour = (SELECT idtour FROM tour WHERE numTour = :numTour AND idTournoi = :idTournoi)) OR idjoueur IN (SELECT idJoueur2 FROM matchs WHERE idtour = (SELECT idtour FROM tour WHERE numTour = :numTour AND idTournoi = :idTournoi))";
      $query = $this->bdd->prepare($sql);
      $query->execute(array('numTour' => $numTour, 'idTournoi' => $idTournament));
      while($line = $query->fetch()) {
          $res[] = new Joueur($line['idJoueur'],$line['nom'],$line['prenom'],$line['age'],$line['nationalite']);
      }

      $query->closecursor();

      return $res;
    }
}

?>
