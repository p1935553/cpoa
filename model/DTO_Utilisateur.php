<?php

class Utilisateur{
    private $id;
    private $username;
    private $admin;
    private $nom;
    private $prenom;
    private $age;
    private $adresse;
    private $cpVille;
    private $mdp;

    public function __construct($id, $username, $admin, $nom, $prenom, $age, $adresse, $cpVille, $mdp) {
        $this->id = $id;
        $this->username = $username;
        $this->admin = $admin;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->age = $age;
        $this->adresse = $adresse;
        $this->cpVille = $cpVille;
        $this->mdp = $mdp;
    }
   
    public function __get($attr) {
        switch($attr) {
            case 'id' :
                return $this->id;
                break;
            case 'username' :
                return $this->username;
                break;
            case 'admin' :
                return $this->admin;
                break;
            case 'nom' :
                return $this->nom;
                break;
            case 'prenom' :
                return $this->prenom;
                break;
            case 'age' :
                return $this->age;
                break;
            case 'adresse' :
                return $this->adresse;
                break;
            case 'cpVille' :
                return $this->cpVille;
                break;
            case 'mdp' :
                return $this->mdp;
                break;
        }
    }

    public function __set($attr, $valeur) {
        switch($attr) {
            case 'id' :
                $this->id = $valeur;
                break;
            case 'username' :
                $this->username = $valeur;
                break;
            case 'admin' :
                $this->admin = $valeur;
                break;
            case 'nom' :
                $this->nom = $valeur;
                break;
            case 'prenom' :
                $this->prenom = $valeur;
                break;
            case 'age' :
                $this->age = $valeur;
                break;
            case 'adresse' :
                $this->adresse = $valeur;
                break;
            case 'cpVille' :
                $this->cpVille = $valeur;
                break;
            case 'mdp' :
                $this->mdp = $valeur;
                break;
        }
    }

    public function isAdmin() {
        return $this->admin;
    }
}

?>