<?php

require_once('variables.php');

class DAO_Tournoi {
    private $bdd;

    public function __construct() {
        try {
            $this->bdd = new PDO("mysql:dbname=".$GLOBALS['dbName'].";host=localhost", $GLOBALS['dbName'], $GLOBALS['dbMdp']);
        }
        catch (PDOException $e) {
            $this->bdd = null;
            die($e->getMessage());
        }
    }

    public function findAll() {
        $line = null;
        $res = null;
        $sql = "select * from tournoi";
        $query = $this->bdd->prepare($sql);
        $query->execute();
        while($line = $query->fetch()) {
            $res[] = new Tournoi($line['idTournoi'],$line['nom'],$line['date'],$line['nbParticipants'],$line['idGagnant']);
        }
        $query->closecursor();
        return $res;
    }

    public function findLike($str) {
        $line = null;
        $res = null;
        $like = '%'.$str.'%';
        $sql = "select * from tournoi where nom like ?";
        $query = $this->bdd->prepare($sql);
        $query->execute(array($like));
        while($line = $query->fetch()) {
            $res[] = new Tournoi($line['idTournoi'],$line['nom'],$line['date'],$line['nbParticipants'],$line['idGagnant']);
        }
        $query->closecursor();
        return $res;
    }

    public function findByID($id) {
        $sql = "select * from tournoi where idTournoi = ?";
        $query = $this->bdd->prepare($sql);
        $query->execute(array($id));
        if ($line = $query->fetch()) {
            return new Tournoi($line['idTournoi'],$line['nom'],$line['date'],$line['nbParticipants'],$line['idGagnant']);
        } else {
            return null;
        }
        $query->closecursor();
    }

    public function deleteById($id)
    {
        $query = $this->bdd->prepare('delete from tournoi where idTournoi = :id');
        $query->execute(array('id' => $id));
    }

    public function addTournoi($nom, $date, $nbparticipants)
    {
        $query = $this->bdd->prepare('insert into tournoi values(DEFAULT, ?, ?, ?, NULL)');
        $query->execute(array($nom, $date, $nbparticipants));
    }

    public function findByAll($nom, $date, $nbparticipants) {
        $query = $this->bdd->prepare('select * from tournoi where nom = ? and date = ? and nbParticipants = ?');
        $query->execute(array($nom, $date, $nbparticipants));
        if ($line = $query->fetch()) {
            return new Tournoi($line['idTournoi'], $line['nom'], $line['date'], $line['nbParticipants'], $line['idGagnant']);
        } else {
            return null;
        }
        $query->closecursor();
    }

    public function findlasttournamentID() {
        $query = $this->bdd->prepare('select max(idTournoi) as max from tournoi ');
        $query->execute();
        $res = $query->fetch();
        $query->closecursor();
        return $res['max'];

    }
}

?>