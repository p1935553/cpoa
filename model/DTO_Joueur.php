<?php

class Joueur{
    private $idJoueur;
    private $nom;
    private $prenom;
    private $age;
    private $nationalite;

    public function __construct($idJoueur, $nom, $prenom, $age, $nationalite) {
        $this->idJoueur = $idJoueur;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->age = $age;
        $this->nationalite = $nationalite;
    }
   
    public function __get($attr) {
        switch($attr) {
            case 'idJoueur' :
                return $this->idJoueur;
                break;
            case 'nom' :
                return $this->nom;
                break;
            case 'prenom' :
                return $this->prenom;
                break;
            case 'age' :
                return $this->age;
                break;
            case 'nationalite' :
                return $this->nationalite;
                break;
        }
    }

    public function __set($attr, $valeur) {
        switch($attr) {
            case 'idJoueur' :
                $this->idJoueur = $valeur;
                break;
            case 'nom' :
                $this->nom = $valeur;
                break;
            case 'prenom' :
                $this->prenom = $valeur;
                break;
            case 'age' :
                $this->age = $valeur;
                break;
            case 'nationalite' :
                $this->nationalite = $valeur;
                break;
        }
    }
}

?>