<?php

require_once('variables.php');

class DAO_Utilisateur {
    private $bdd;

    public function __construct() {
        try {
            $this->bdd = new PDO("mysql:dbname=".$GLOBALS['dbName'].";host=localhost", $GLOBALS['dbName'], $GLOBALS['dbMdp']);
        }
        catch (PDOException $e) {
            $this->bdd = null;
            die($e->getMessage());
        }
    }

    public function findAll() {
        $line = null;
        $res = null;
        $sql = "select * from utilisateur";
        $query = $this->bdd->prepare($sql);
        $query->execute();
        while($line = $query->fetch()) {
            $res[] = new Utilisateur($line['id'],$line['username'],$line['admin'],$line['nom'],$line['prenom'],$line['age'],$line['adresse'],$line['cpVille'],$line['mdp']);
        }
        $query->closecursor();
        return $res;
    }

    public function findByUser($user) {
        $sql = "select * from utilisateur where username = ?";
        $query = $this->bdd->prepare($sql);
        $query->execute(array($user));
        if ($line = $query->fetch()) {
            return new Utilisateur($line['id'],$line['username'],$line['admin'],$line['nom'],$line['prenom'],$line['age'],$line['adresse'],$line['cpVille'],$line['mdp']);
        } else {
            return null;
        }
        $query->closeCursor();
    }

    public function addUser($username, $admin, $nom, $prenom, $adresse, $cpVille, $age, $mdp)
    {
        $query = $this->bdd->prepare('insert into utilisateur values(DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?)');
        $query->execute(array($username, $admin, $nom, $prenom, $age, $adresse, $cpVille, sha1($mdp)));
    }

    public function updateUserbyUsername($id, $username, $admin, $nom, $prenom, $adresse, $cpVille, $age, $mdp)
    {
        $query = $this->bdd->prepare('update utilisateur set username = ?, admin = ?, nom = ?, prenom = ?, adresse = ?, cpVille = ?, age = ?, mdp = ? where id = ?');
        $query->execute(array($username, $admin, $nom, $prenom, $adresse, $cpVille, $age, sha1($mdp), $id));
    }

    public function updateUserWithoutPswd($id, $username, $admin, $nom, $prenom, $adresse, $cpVille, $age)
    {
        $query = $this->bdd->prepare('update utilisateur set username = ?, admin = ?, nom = ?, prenom = ?, adresse = ?, cpVille = ?, age = ? where id = ?');
        $query->execute(array($username, $admin, $nom, $prenom, $adresse, $cpVille, $age, $id));
    }

    public function deleteById($id)
    {
        $query = $this->bdd->prepare('delete from utilisateur where id = :id');
        $query->execute(array('id' => $id));
    }
}

?>