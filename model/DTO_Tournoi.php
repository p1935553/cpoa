<?php

class Tournoi {
    private $idTournoi;
    private $nom;
    private $date;
    private $nbParticipants;
    private $idGagnant;

    public function __construct($idTournoi, $nom, $date, $nbParticipants, $idGagnant) {
        $this->idTournoi = $idTournoi;
        $this->nom = $nom;
        $this->date = $date;
        $this->nbParticipants = $nbParticipants;
        $this->idGagnant = $idGagnant;
    }
   
    public function __get($attr) {
        switch($attr) {
            case 'idTournoi' :
                return $this->idTournoi;
                break;
            case 'nom' :
                return $this->nom;
                break;
            case 'date' :
                return $this->date;
                break;
            case 'nbParticipants' :
                return $this->nbParticipants;
                break;
            case 'idGagnant' :
                return $this->idGagnant;
                break;
        }
    }

    public function __set($attr, $valeur) {
        switch($attr) {
            case 'idTournoi' :
                $this->idTournoi = $valeur;
                break;
            case 'nom' :
                $this->nom = $valeur;
                break;
            case 'date' :
                $this->date = $valeur;
                break;
            case 'nbParticipants' :
                $this->nbParticipants = $valeur;
                break;
            case 'idGagnant' :
                $this->idGagnant = $valeur;
                break;
        }
    }
}

?>