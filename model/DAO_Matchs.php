<?php

require_once('variables.php');

class DAO_Matchs {
    private $bdd;

    public function __construct() {
        try {
            $this->bdd = new PDO("mysql:dbname=".$GLOBALS['dbName'].";host=localhost", $GLOBALS['dbName'], $GLOBALS['dbMdp']);
        }
        catch (PDOException $e) {
            $this->bdd = null;
            die($e->getMessage());
        }
    }

    public function findAll() {
        $line = null;
        $res = null;
        $sql = "select * from matchs";
        $query = $this->bdd->prepare($sql);
        $query->execute();
        while($line = $query->fetch()) {
            $res[] = new Matchs($line['idMatch'],$line['idTour'],$line['date'],$line['idJoueur1'],$line['idJoueur2'],$line['score1'],$line['score2'],$line['idGagnant']);
        }
        $query->closecursor();
        return $res;
    }

    public function findById($id) {
        $sql = "select * from matchs where idMatch = ?";
        $query = $this->bdd->prepare($sql);
        $query->execute(array($id));
        if ($line = $query->fetch()) {
            return new Matchs($line['idMatch'],$line['idTour'],$line['date'],$line['idJoueur1'],$line['idJoueur2'],$line['score1'], $line['score2'],$line['idGagnant']);
        } else {
            return null;
        }
        $query->closecursor();
    }

    public function findByRound($id) {
        $sql = "select * from matchs where idTour = ?";
        $query = $this->bdd->prepare($sql);
        $query->execute(array($id));
        $matchs = null;
        while ($line = $query->fetch()) {
            $matchs[] = new Matchs($line['idMatch'], $line['idTour'], $line['date'], $line['idJoueur1'], $line['idJoueur2'], $line['score1'], $line['score2'], $line['idGagnant']);
        }
        $query->closecursor();
        return $matchs;
    }

    public function addMatch($idTour, $date, $idJoueur1, $idJoueur2)
    {
        $query = $this->bdd->prepare('insert into matchs values(DEFAULT, ?, ?, ?, ?, NULL, NULL, NULL)');
        $query->execute(array($idTour, $date, $idJoueur1, $idJoueur2));
    }

    public function deleteByRoundID($id)
    {
        $query = $this->bdd->prepare('delete from matchs where idTour = :id');
        $query->execute(array('id' => $id));
    }

    public function updateMatch($idMatch, $score1, $score2) {
        $query = $this->bdd->prepare('update matchs set score1 = :score1, score2 = :score2, idGagnant = :win where idMatch = :id');
        if ($score1 > $score2) {
            $query->execute(array('id' => $idMatch, 'score1' => $score1, 'score2' => $score2, 'win' => ($this->findById($idMatch)->idJoueur1)));
        }
        else {
            $query->execute(array('id' => $idMatch, 'score1' => $score1, 'score2' => $score2, 'win' => ($this->findById($idMatch)->idJoueur2)));
        }
    }
}

?>