<?php

class Tour{
    private $idTour;
    private $idTournoi;
    private $numTour;

    public function __construct($idTour, $idTournoi,$numTour) {
        $this->idTour = $idTour;
        $this->idTournoi = $idTournoi;
        $this->numTour = $numTour;
    }
   
    public function __get($attr) {
        switch($attr) {
            case 'idTour' :
                return $this->idTour;
                break;
            case 'idTournoi' :
                return $this->idTournoi;
                break;
            case 'numTour' :
                return $this->numTour;
                break;
        }
    }

    public function __set($attr, $valeur) {
        switch($attr) {
            case 'idTour' :
                $this->idTour = $valeur;
                break;
            case 'idTournoi' :
                $this->idTournoi = $valeur;
                break;
            case 'numTour' :
                $this->numTour = $valeur;
                break;
        }
    }
}

?>