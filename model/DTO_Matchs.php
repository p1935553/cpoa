<?php

class Matchs{
    private $idMatch;
    private $idTour;
    private $idJoueur1;
    private $idJoueur2;
    private $score1;
    private $score2;
    private $idGagnant;

    public function __construct($idMatch, $idTour, $date, $idJoueur1, $idJoueur2, $score1, $score2, $idGagnant) {
        $this->idMatch = $idMatch;
        $this->idTour = $idTour;
        $this->date = $date;
        $this->idJoueur1 = $idJoueur1;
        $this->idJoueur2 = $idJoueur2;
        $this->score1 = $score1;
        $this->score2 = $score2;
        $this->idGagnant = $idGagnant;
    }

    public function __get($attr) {
        switch($attr) {
            case 'idMatch' :
                return $this->idMatch;
                break;
            case 'idTour' :
                return $this->idTour;
                break;
            case 'idJoueur1' :
                return $this->idJoueur1;
                break;
            case 'idJoueur2' :
                return $this->idJoueur2;
                break;
            case 'score1':
                return $this->score1;
                break;
            case 'score2':
                return $this->score2;
                break;
            case 'idGagnant' :
                return $this->idGagnant;
                break;
            case 'date' :
                return $this->date;
                break;
        }
    }

    public function __set($attr, $valeur) {
        switch($attr) {
            case 'idMatch' :
                $this->idMatch = $valeur;
                break;
            case 'idTour' :
                break;
            case 'idJoueur1' :
                $this->idJoueur1 = $valeur;
                break;
            case 'idJoueur2' :
                $this->idJoueur2 = $valeur;
                break;
            case 'score1':
                $this->score1 = $valeur;
                break;
            case 'score2':
                $this->score2 = $valeur;
                break;
            case 'idGagnant' :
                $this->idGagnant = $valeur;
                break;
            case 'date' :
                $this->date = $valeur;
                break;    
        }
    }
}

?>
