<?php

require_once('variables.php');

class DAO_Tour {
    private $bdd;

    public function __construct() {
        try {
            $this->bdd = new PDO("mysql:dbname=".$GLOBALS['dbName'].";host=localhost", $GLOBALS['dbName'], $GLOBALS['dbMdp']);
        }
        catch (PDOException $e) {
            $this->bdd = null;
            die($e->getMessage());
        }
    }

    public function findAll() {
        $line = null;
        $res = null;
        $sql = "select * from tour";
        $query = $this->bdd->prepare($sql);
        $query->execute();
        while($line = $query->fetch()) {
            $res[] = new Tour($line['idTour'],$line['idTournoi'],$line['numTour']);
        }
        $query->closecursor();
        return $res;
    }

    public function findByPlayerID($id) {
        $line = null;
        $res = null;
        $sql = "select * from tour where idTour = ?";
        $query = $this->bdd->prepare($sql);
        $query->execute(array($id));
        while($line = $query->fetch()) {
            $res = new Tour($line['idTour'],$line['idTournoi'],$line['numTour']);
        }
        $query->closecursor();
        return $res;
    }

    public function findByTournamentID($id) {
        $line = null;
        $res = null;
        $sql = "select * from tour where idTournoi = ? order by numTour asc";
        $query = $this->bdd->prepare($sql);
        $query->execute(array($id));
        while($line = $query->fetch()) {
            $res[] = new Tour($line['idTour'],$line['idTournoi'],$line['numTour']);
        }
        $query->closecursor();
        return $res;
    }

    public function addTour($idTournoi, $numTour)
    {
        $query = $this->bdd->prepare('insert into tour values(DEFAULT, ?, ?)');
        $query->execute(array($idTournoi, $numTour));
    }

    public function findByTournamentIDandnumTournoi($id, $numTour)
    {
        $sql = "select * from tour where idTournoi = ? and numTour = ?";
        $query = $this->bdd->prepare($sql);
        $query->execute(array($id,$numTour));
        if ($line = $query->fetch()) {
            return new Tour($line['idTour'],$line['idTournoi'],$line['numTour']);
        } else {
            return null;
        }
        $query->closecursor();
    }

    public function deleteByTournamentID($id)
    {
        $query = $this->bdd->prepare('delete from tour where idTournoi = :id');
        $query->execute(array('id' => $id));
    }
}

?>