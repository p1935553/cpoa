<?php

// Fichiers nécessaires à la gestion des données

//require __DIR__.'/model/file.php';
// Variables globales
require_once('variables.php');
require_once('model/DAO_Tournoi.php');
require_once('model/DTO_Tournoi.php');
require_once('model/DAO_Joueur.php');
require_once('model/DTO_Joueur.php');
require_once('model/DAO_Utilisateur.php');
require_once('model/DTO_Utilisateur.php');
require_once('model/DAO_Tour.php');
require_once('model/DTO_Tour.php');
require_once('model/DAO_Matchs.php');
require_once('model/DTO_Matchs.php');

// Démarrage de la session pour le stockage des données par utilisateur unique
session_start();



// Analyse de la requête et découpage de celle-ci
$uri_args = explode('/', substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], 'index.php') + 10))[0];
if(!strpos($uri_args, '?')) {
    $request = $uri_args;
} else {
    $request = explode('?', $uri_args, -1)[0];
}

// Header dynamique
if(isset($_SESSION['user'])) {
    $connection_button = '<a href="'.$GLOBALS['path'].'/index.php/signout">
    <button type="button" class="btn btn-outline-light me-2">Déconnexion</button>
  </a>';

} else {
    $connection_button = '<a href="'.$GLOBALS['path'].'/index.php/signin">
    <button type="button" class="btn btn-outline-light me-2">Connexion</button>
  </a>';
}

// Redirections du contrôleur
switch ($request) {
    // Accueil du site
    case '' :
        require __DIR__ . '/view/assets/header.php';
        require 'brackets.php';
        // Vérification de connexion. $_SESSION['user'] contient un objet Utilisateur.
        if (isset($_SESSION['user'])) {
            if ($_SESSION['user']->isAdmin()) {
                require __DIR__ . '/view/admin.php';
            } else {
                require __DIR__ . '/view/referee.php';
            }
        } else {
            $dao_tournoi = new DAO_Tournoi;
            $tournois = $dao_tournoi->findAll();
            $dao_joueur = new DAO_Joueur;
            $joueurs_data = $dao_joueur->findAll();
            for($i = 0; $i < count($joueurs_data); $i++) {
                $joueurs[$i]['joueur'] = $joueurs_data[$i];
                $joueurs[$i]['victoires'] = $dao_joueur->countVictories($joueurs_data[$i]->idJoueur);
                $joueurs[$i]['defaites'] = $dao_joueur->countDefeats($joueurs_data[$i]->idJoueur);
            }
            echo '<h1>Dernier tournoi</h1>';
            $tournament = generateTournament($dao_tournoi->findlasttournamentID());
            displayTournament($tournament);
            require __DIR__ . '/view/home.php';
        }
        require __DIR__ . '/view/assets/footer.php';
        break;
    case 'search' :
        require __DIR__ . '/view/assets/header.php';
        require __DIR__ . '/view/search.php';
        require __DIR__ . '/view/assets/footer.php';
        break;
    //page de connexion
    case 'signin' :
        if (isset($_POST['username']) && isset($_POST['password']) && !empty($_POST['username']) && !empty($_POST['password'])) {
            $dao_utilisateur = new DAO_Utilisateur();
            $utilisateur = $dao_utilisateur->findByUser(htmlspecialchars($_POST['username']));
            if ($utilisateur == null) {
                $_SESSION['info'] = 'Le nom d\'utilisateur est erroné';
                header('Location:' . $path .'/index.php/signin');
            } else {
                if (hash_equals($utilisateur->mdp, sha1($_POST['password']))) {
                    $_SESSION['info'] = 'Connecté';
                    $_SESSION['user'] = $utilisateur;
                    header('Location: ' . $path . '/index.php');
                } else {
                    $_SESSION['info'] = 'Le mot de passe est erroné';
                    header('Location:' . $path .'/index.php/signin');
                }
            }
        } else {
            require __DIR__ . '/view/assets/header.php';
            require __DIR__ . '/view/signin.php';
            require __DIR__ . '/view/assets/footer.php';
        }
        break;
    case 'signout' :
        unset($_SESSION);
        unset($_COOKIE);
        session_destroy();
        session_start();
        header('Location:' . $path .'/index.php');
        break;
    case 'add': // Ajouts de données
        //ajout d'un tournoi
        if (!empty($_POST['nom']) && !empty($_POST['date']) && !empty($_POST['nbparticipants']) && $_SESSION['user']->isAdmin()) {
            if ((int)htmlspecialchars($_POST['nbparticipants']) <= 16) {
                $dao_tournoi = new DAO_Tournoi();
                $dao_joueur = new DAO_Joueur();
                $joueurs_all = $dao_joueur->findAll();
                for($i = 0; $i < count($joueurs_all); $i++) {
                    if($joueurs_all[$i]->idJoueur == -1) unset($joueurs_all[$i]);
                }
                // Vérifications et tirage au sort des joueurs
                if(count($joueurs_all) >= (int)htmlspecialchars($_POST['nbparticipants'])  && (int)htmlspecialchars($_POST['nbparticipants']) != 0 && ((int)htmlspecialchars($_POST['nbparticipants']) & ((int)htmlspecialchars($_POST['nbparticipants']) - 1)) == 0) {
                    $dao_tournoi->addTournoi(htmlspecialchars($_POST['nom']), htmlspecialchars($_POST['date']), htmlspecialchars($_POST['nbparticipants']));
                    $tournoi = $dao_tournoi->findByAll(htmlspecialchars($_POST['nom']), htmlspecialchars($_POST['date']), htmlspecialchars($_POST['nbparticipants']));
                    $joueurs_keys = array_rand($joueurs_all, (int)htmlspecialchars($_POST['nbparticipants']));
                    
                    foreach($joueurs_keys as $key) {
                        $joueurs[] = $joueurs_all[$key];
                    }
                    
                    // Création de tous les tours du tournoi, en fonction du nb de participants
                    $dao_tour = new DAO_Tour();
                    $numTour = 1;
                    $nbParti = $_POST['nbparticipants'];
                    while($nbParti > 1) {
                        $dao_tour->addTour($tournoi->idTournoi, $numTour);
                        $numTour = $numTour+1;
                        $nbParti = $nbParti/2;
                    }
                    // Création de tous les matchs du tournoi
                    $tours = $dao_tour->findByTournamentID($tournoi->idTournoi);
                    $dao_matchs = new DAO_Matchs();
                    $nbPartiTournoi = $_POST['nbparticipants'];
                    $nbPartiTour = $nbPartiTournoi;
                    $i = 0;
                    foreach($tours as $tour) {
                        while($nbPartiTournoi > 1) {
                            if ($tour->numTour == 1) {
                                $dao_matchs->addMatch($tour->idTour, htmlspecialchars($_POST['date']), $joueurs[$i]->idJoueur, $joueurs[$i+1]->idJoueur);
                            } else {
                                $dao_matchs->addMatch($tour->idTour, htmlspecialchars($_POST['date']), -1, -1);
                            }
                            $nbPartiTournoi = $nbPartiTournoi-2;
                            $i = $i+2;
                        }
                        $nbPartiTournoi = $nbPartiTour/2;
                        $nbPartiTour = $nbPartiTour/2;
                    }
                    $_SESSION['info'] = 'Tournoi créé';
                    header('Location: ' . $path . '/index.php');
                } else {
                    $_SESSION['info'] = 'Trop peu de joueurs pour créer le tournoi';
                    header('Location: ' . $path . '/index.php');
                }
            } else {
                $_SESSION['info'] = 'Il faut 16 joueurs ou moins';
                header('Location: ' . $path . '/index.php');
            }
        } else {
            $_SESSION['info'] = 'Merci de remplir tous les champs';
            header('Location: ' . $path . '/index.php');
        }
        //ajout d'un joueur
        if (!empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['age']) && !empty($_POST['nationalite']) && $_SESSION['user']->isAdmin()) {
            $dao_joueur = new DAO_Joueur();
            $dao_joueur->addPlayer(htmlspecialchars($_POST['nom']), htmlspecialchars($_POST['prenom']), htmlspecialchars($_POST['age']), htmlspecialchars($_POST['nationalite']));
            $_SESSION['info'] = 'Joueur créé';
            header('Location: ' . $path . '/index.php');
        }
        //ajout d'un utilisateur
        if (!empty($_POST['username']) && !empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['adresse']) && !empty($_POST['cpVille']) && !empty($_POST['age']) && !empty($_POST['mdp']) && !empty($_POST['mdp2']) && $_SESSION['user']->isAdmin()) {
            $dao_utilisateur = new DAO_Utilisateur();
            if (hash_equals($_POST['mdp'], $_POST['mdp2'])) {
                $dao_utilisateur->addUser($_POST['username'], $_POST['admin'], $_POST['nom'], $_POST['prenom'], $_POST['adresse'], $_POST['cpVille'], $_POST['age'], $_POST['mdp']);
                $_SESSION['info'] = 'Utilisateur créé';
                header('Location: ' . $path . '/index.php');
            }
            else {
                $_SESSION['info'] = 'Les mots de passe ne correspondent pas';
                header('Location: ' . $path . '/index.php');
            }
        }
        break;
    case "add_scores"; //ajout des scores par l'arbitre
        if (!empty($_POST['idMatch']) && isset($_POST['score1']) && isset($_POST['score2']) && !$_SESSION['user']->isAdmin()) {
            $dao_matchs = new DAO_Matchs();
            $dao_matchs->updateMatch($_POST['idMatch'], $_POST['score1'], $_POST['score2']);
            $bdd = new PDO("mysql:dbname=".$GLOBALS['dbName'].";host=localhost", $GLOBALS['dbName'], $GLOBALS['dbMdp']);
            $query = $bdd->prepare('SELECT idTournoi FROM tour WHERE idTour = (SELECT idTour FROM matchs WHERE idMatch = :id)');
            $query->execute(array('id' => $_POST['idMatch']));
            $idTournoi = $query->fetch()['idTournoi'];
            $query = $bdd->prepare('SELECT idTour FROM tour WHERE numTour = (SELECT MAX(numTour) FROM tour WHERE idTournoi = :id) AND idTournoi = :id');
            $query->execute(array('id' => $idTournoi));
            $maxIdTour = $query->fetch()['idTour'];
            $query = $bdd->prepare('SELECT idTour FROM matchs WHERE idMatch = :id');
            $query->execute(array('id' => $_POST['idMatch']));
            $idTour = $query->fetch()['idTour'];
            if($idTour != $maxIdTour) {
                $query = $bdd->prepare('SELECT MIN(idMatch) as minId FROM matchs WHERE idTour = :id');
                $query->execute(array('id' => $idTour));
                $minIdMatchRoundA = $query->fetch()['minId'];
                $query = $bdd->prepare('SELECT MIN(idMatch) as minId FROM matchs WHERE idTour = :id');
                $query->execute(array('id' => ((int)$idTour+1)));
                $minIdMatchRoundB = $query->fetch()['minId'];
                $idMatchRelatifA = (int)$_POST['idMatch'] - (int)$minIdMatchRoundA;
                $idMatchRelatifB = intdiv($idMatchRelatifA, 2);
                $idMatchB = (int)$idMatchRelatifB + (int)$minIdMatchRoundB;
                if($_POST['score1'] > $_POST['score2']) {
                    $query = $bdd->prepare('SELECT idJoueur1 FROM matchs WHERE idMatch = :id');
                    $query->execute(array('id' => $_POST['idMatch']));
                    $idJoueur = $query->fetch()['idJoueur1'];
                } else {
                    $query = $bdd->prepare('SELECT idJoueur2 FROM matchs WHERE idMatch = :id');
                    $query->execute(array('id' => $_POST['idMatch']));
                    $idJoueur = $query->fetch()['idJoueur2'];
                }
                if($idMatchRelatifA%2 == 0) {
                    $query = $bdd->prepare('UPDATE matchs SET idJoueur1 = :idJoueur WHERE idMatch = :id');
                    $query->execute(array('idJoueur' => $idJoueur, 'id' => $idMatchB));
                } else {
                    $query = $bdd->prepare('UPDATE matchs SET idJoueur2 = :idJoueur WHERE idMatch = :id');
                    $query->execute(array('idJoueur' => $idJoueur, 'id' => $idMatchB));
                }
            } else {
                if($_POST['score1'] > $_POST['score2']) {
                    $query = $bdd->prepare('SELECT idJoueur1 FROM matchs WHERE idMatch = :id');
                    $query->execute(array('id' => $_POST['idMatch']));
                    $idJoueur = $query->fetch()['idJoueur1'];
                } else {
                    $query = $bdd->prepare('SELECT idJoueur2 FROM matchs WHERE idMatch = :id');
                    $query->execute(array('id' => $_POST['idMatch']));
                    $idJoueur = $query->fetch()['idJoueur2'];
                }
                $query = $bdd->prepare('UPDATE tournoi SET idGagnant = :idG WHERE idTournoi = :id');
                $query->execute(array('idG' => $idJoueur,'id' => $idTournoi));
            }
            $_SESSION['info'] = 'Les scores ont bien été pris en compte';
            header('Location: ' . $path . '/index.php');
        } else {
            $_SESSION['info'] = 'Les valeurs sont incorrecteeeeeees';
            header('Location: ' . $path . '/index.php');
        }
        break;
    case 'modify': 
        // modification d'un joueur
        if (!empty($_POST['idJoueur']) && !empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['age']) && !empty($_POST['nationalite']) && $_SESSION['user']->isAdmin()) {
            $dao_joueur = new DAO_Joueur();
            $dao_joueur->updatePlayerById(htmlspecialchars($_POST['idJoueur']), htmlspecialchars($_POST['nom']), htmlspecialchars($_POST['prenom']), htmlspecialchars($_POST['age']), htmlspecialchars($_POST['nationalite']));
            $_SESSION['info'] = 'Joueur modifié';
            header('Location: ' . $path . '/index.php');
        }
        // modification d'un utilisateur
        if (!empty($_POST['username']) && !empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['adresse']) && !empty($_POST['cpVille']) && !empty($_POST['age']) && $_SESSION['user']->isAdmin()) {
            $dao_utilisateur = new DAO_Utilisateur();
            if (!empty($_POST['mdp']) && !empty($_POST['mdp2'])) {
                if (hash_equals($_POST['mdp'], $_POST['mdp2'])) {
                    $dao_utilisateur->updateUserbyUsername($_POST['id'], $_POST['username'], $_POST['admin'], $_POST['nom'], $_POST['prenom'], $_POST['adresse'], $_POST['cpVille'], $_POST['age'], $_POST['mdp']);
                    $_SESSION['info'] = 'Utilisateur modifié';
                    header('Location: ' . $path . '/index.php');
                }
                else {
                    $_SESSION['info'] = 'Les mots de passe ne correspondent pas';
                    header('Location: ' . $path . '/index.php');
                }
            } else if ((!empty($_POST['mdp']) && empty($_POST['mdp2'])) || (empty($_POST['mdp']) && !empty($_POST['mdp2']))) {
                $_SESSION['info'] = 'Il faut remplir les deux champs de mot de passe';
                header('Location: ' . $path . '/index.php');
            } else {
                $dao_utilisateur->updateUserWithoutPswd($_POST['id'], $_POST['username'], $_POST['admin'], $_POST['nom'], $_POST['prenom'], $_POST['adresse'], $_POST['cpVille'], $_POST['age']);
                $_SESSION['info'] = 'Utilisateur modifié';
                header('Location: ' . $path . '/index.php');
            }
        }
        break;
    case 'delete_player': // Suppression de données
        // joueurs
        if (!empty($_GET['id']) && $_SESSION['user']->isAdmin()) {
            $dao_joueur = new DAO_Joueur();
            $dao_joueur->deleteById(htmlspecialchars($_GET['id']));
            $_SESSION['info'] = 'Joueur supprimé';
            header('Location: ' . $path . '/index.php');
        }
        break;
    case 'delete_user': // Suppression de données
        //utilisateurs
        if (!empty($_GET['id']) && $_SESSION['user']->isAdmin()) {
            $dao_utilisateur = new DAO_Utilisateur();
            $dao_utilisateur->deleteById(htmlspecialchars($_GET['id']));
            $_SESSION['info'] = 'Utilisateur supprimé';
            header('Location: ' . $path . '/index.php');
        }
        break;
    case 'delete_tournament': // Suppression de données
        //Tournois, tours et matchs associés
        if (!empty($_GET['id']) && $_SESSION['user']->isAdmin()) {
            $dao_tournoi = new DAO_Tournoi();
            $dao_tournoi->deleteById(htmlspecialchars($_GET['id']));
            $dao_tour = new DAO_Tour();
            $tours = $dao_tour->findByTournamentID(htmlspecialchars($_GET['id']));
            $dao_tour->deleteByTournamentID(htmlspecialchars($_GET['id']));
            $dao_matchs = new DAO_Matchs();
            foreach($tours as $tour) {
                $dao_matchs->deleteByRoundID($tour->idTour);
            }
            $_SESSION['info'] = 'Tournoi supprimé';
            header('Location: ' . $path . '/index.php');
        }
        break;
    case "show"; //affichage joueur et tournoi
        break;
    default: // Affichage par défaut (utilisé en cas d'accès à des données interdites
        require __DIR__ . '/view/assets/header.php';
        var_dump($request);
        http_response_code(404);
        require __DIR__ . '/view/error.php';
        require __DIR__ . '/view/assets/footer.php';
        break;
}
