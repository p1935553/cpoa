<!-- Dernier tournoi -->


<!-- Accordéon -->
<h1>Ressources</h1>
<div class="accordion" id="mainAccordion">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
        Tournois
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#mainAccordion">
      <div class="accordion-body">
      <ul class="list-group">
        <?php
            if(!empty($tournois)) {
              foreach($tournois as $tournoi) {
                echo '<li class="list-group-item"><span>'.$tournoi->nom.'</span><button type="button" class="btn btn-sm btn-secondary" data-bs-toggle="modal" data-bs-target="#modalTournoi'.$tournoi->idTournoi.'">Voir plus <i data-feather="arrow-right"></i></button></li>
                <!-- Modal -->
                <div class="modal fade" id="modalTournoi'.$tournoi->idTournoi.'" tabindex="-1" aria-labelledby="modalTournoi'.$tournoi->idTournoi.'Label" aria-hidden="true">
                <div class="modal-dialog" style="max-width: 1000px;">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalTournoi'.$tournoi->idTournoi.'Label">Détails tournoi</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">';
                    $tournament = generateTournament($tournoi->idTournoi);
                    displayTournament($tournament);
                    echo '</div>
                    </div>
                </div>
                </div>';
              }
            } else {
              echo '<li class="list-group-item"><span>Aucun tournoi disponible</span></li>';
            }
        ?>
      </ul>
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        Joueurs
      </button>
    </h2>
    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#mainAccordion">
      <div class="accordion-body">
      <ul class="list-group">
        <?php
            if(!empty($joueurs)) {
              foreach($joueurs as $joueur) {
                echo '<li class="list-group-item"><span>'.$joueur['joueur']->prenom. ' ' .$joueur['joueur']->nom.'</span><button type="button" class="btn btn-sm btn-secondary" data-bs-toggle="modal" data-bs-target="#modalJoueur'.$joueur['joueur']->idJoueur.'">Voir plus <i data-feather="arrow-right"></i></button></li>
                <!-- Modal -->
                <div class="modal fade" id="modalJoueur'.$joueur['joueur']->idJoueur.'" tabindex="-1" aria-labelledby="modalJoueur'.$joueur['joueur']->idJoueur.'Label" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalJoueur'.$joueur['joueur']->idJoueur.'Label">Détails joueur</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <h3>'.$joueur['joueur']->nom.' '.$joueur['joueur']->prenom.'</h3>
                        <h5>Age : '.$joueur['joueur']->age.' ans</h5>
                        <h5>Nationalité : '.$joueur['joueur']->nationalite.'</h5>
                        <h5>Victoire(s) : '.$joueur['victoires'].'</h5>
                    </div>
                    </div>
                </div>
                </div>';
              }
            } else {
              echo '<li class="list-group-item"><span>Aucun joueur disponible</span></li>';
            }
        ?>
      </ul>
      </div>
    </div>
  </div>
</div>