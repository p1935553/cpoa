<?php
require_once('variables.php');
require_once('model/DAO_Tournoi.php');
require_once('model/DTO_Tournoi.php');
require_once('model/DAO_Joueur.php');
require_once('model/DTO_Joueur.php');
require_once('model/DAO_Utilisateur.php');
require_once('model/DTO_Utilisateur.php');
?>
<h1>Interface Administrateur</h1>
<!-- Accordéon -->
<div class="accordion" id="mainAccordion">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
        Tournois
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#mainAccordion">
      <div class="accordion-body">
      <ul class="list-group">
          <li class="list-group-item" style="justify-content:center !important;"><button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#modalTournoiAjout">Ajouter un tournoi <i data-feather="plus"></i></button></li>
            <!-- Modal -->
            <div class="modal fade" id="modalTournoiAjout" tabindex="-1" aria-labelledby="modalTournoiAjoutLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTournoiAjoutLabel">Nouveau tournoi</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                <form method="post" action="<?= $path ?>/index.php/add">
                  <div class="mb-3">
                    <label for="newTournoiName" class="form-label">Nom</label>
                    <input type="text" class="form-control" name="nom" id="newTournoiName">
                  </div>
                  <div class="mb-3">
                    <label for="newTournoiDate" class="form-label">Date</label>
                    <input type="date" class="form-control" name="date" id="newTournoiDate">
                  </div>
                  <div class="mb-3">
                    <label for="newTournoiNumber" class="form-label">Nombre de participants</label>
                    <input type="number" class="form-control" name="nbparticipants" id="newTournoiNumber">
                  </div>
                  <button type="submit" class="btn btn-primary">Enregistrer</button>
                </form>
                </div>
                </div>
            </div>
            </div>
            <?php
            $dao_tournoi = new DAO_Tournoi();
            $tournois = $dao_tournoi->findAll();
            if ($tournois == null) {
              echo 'Aucun Tournoi dans la BD';
            } else {
              for($i=0; $i<sizeof($tournois); $i++) {
                echo '<li class="list-group-item"><span>'.$tournois[$i]->nom.'</span><span><button type="button" class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#modalDeleteTournoi'.$i.'"><i data-feather="trash"></i></button>&#8239;<button type="button" class="btn btn-sm btn-secondary" data-bs-toggle="modal" data-bs-target="#modalSeeTournoi'.$i.'">Voir plus <i data-feather="arrow-right"></i></button></span></li>
                <!-- Modal -->
                <div class="modal fade" id="modalDeleteTournoi'.$i.'" tabindex="-1" aria-labelledby="modalDeleteTournoi'.$i.'Label" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="modalDeleteTournoi'.$i.'Label">Suppression</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div class="modal-body">
                        Êtes-vous sûr de vouloir supprimer définitivement ce tournoi ?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                        <a href="'.$path.'/index.php/delete_tournament?id='.$tournois[$i]->idTournoi.'"><button type="button" class="btn btn-danger">Supprimer</button></a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="modalSeeTournoi'.$i.'" tabindex="-1" aria-labelledby="modalSeeTournoi'.$i.'Label" aria-hidden="true">
                <div class="modal-dialog" style="max-width: 1000px;">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalSeeTournoi'.$i.'Label">Tournoi</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">';
                    $tournament = generateTournament($tournois[$i]->idTournoi);
                    displayTournament($tournament);
                    echo '</div>
                    </div>
                </div>
                </div>';
            }
          }
        ?>
      </ul>
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        Joueurs
      </button>
    </h2>
    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#mainAccordion">
      <div class="accordion-body">
      <ul class="list-group">
            <li class="list-group-item" style="justify-content:center !important;"><button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#modalPlayerAjout">Ajouter un joueur <i data-feather="plus"></i></button></li>
            <!-- Modal -->
            <div class="modal fade" id="modalPlayerAjout" tabindex="-1" aria-labelledby="modalPlayerAjoutLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalPlayerAjoutLabel">Ajouter joueur</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                <form method="post" action="<?= $path ?>/index.php/add">
                  <div class="mb-3">
                    <label for="newJoueurName" class="form-label">Nom</label>
                    <input type="text" class="form-control" name="nom" id="newJoueurName">
                  </div>
                  <div class="mb-3">
                    <label for="newJoueurFirstName" class="form-label">Prénom</label>
                    <input type="text" class="form-control" name="prenom" id="newJoueurFirstName">
                  </div>
                  <div class="mb-3">
                    <label for="newJoueurAge" class="form-label">Age</label>
                    <input type="number" class="form-control" name="age" id="newJoueurAge">
                  </div>
                  <div class="mb-3">
                    <label for="newJoueurNationality" class="form-label">Nationalité</label>
                    <input type="text" class="form-control" name="nationalite" id="newJoueurNationality">
                  </div>
                  <button type="submit" class="btn btn-primary">Enregistrer</button>
                </form>
                </div>
                </div>
            </div>
            </div>
            <?php
            $dao_joueur = new DAO_Joueur();
            $joueurs = $dao_joueur->findAll();
            if ($joueurs == null) {
              echo 'Aucun joueur dans la BD';
            } else {
              for($i=0; $i<sizeof($joueurs); $i++) {
                echo '<li class="list-group-item"><span>'.$joueurs[$i]->prenom.' '.$joueurs[$i]->nom.'</span><span><button type="button" class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#modalDeletePlayer'.$i.'"><i data-feather="trash"></i></button>&#8239;<button type="button" class="btn btn-sm btn-secondary" data-bs-toggle="modal" data-bs-target="#modalPlayer'.$i.'">Modifier <i data-feather="edit"></i></button></span></li>
                <!-- Modal -->
                <div class="modal fade" id="modalDeletePlayer'.$i.'" tabindex="-1" aria-labelledby="modalDeletePlayer'.$i.'Label" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="modalDeletePlayer'.$i.'Label">Suppression</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div class="modal-body">
                        Êtes-vous sûr de vouloir supprimer définitivement ce joueur ?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                        <a href="'.$path.'/index.php/delete_player?id='.$joueurs[$i]->idJoueur.'"><button type="button" class="btn btn-danger">Supprimer</button></a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="modalPlayer'.$i.'" tabindex="-1" aria-labelledby="modalPlayer'.$i.'Label" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalPlayer'.$i.'Label">Joueur '.$joueurs[$i]->idJoueur.'</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    <form method="post" action="'. $path .'/index.php/modify">
                      <div class="mb-3">
                        <label for="editJoueurName" class="form-label">Nom</label>
                        <input type="text" class="form-control" name="nom" id="editJoueurName" value="'.$joueurs[$i]->nom.'">
                      </div>
                      <div class="mb-3">
                        <label for="editJoueurFirstName" class="form-label">Prénom</label>
                        <input type="text" class="form-control" name="prenom" id="editJoueurFirstName" value="'.$joueurs[$i]->prenom.'">
                      </div>
                      <div class="mb-3">
                        <label for="editJoueurAge" class="form-label">Age</label>
                        <input type="number" class="form-control" name="age" id="editJoueurAge" value="'.$joueurs[$i]->age.'">
                      </div>
                      <div class="mb-3">
                        <label for="editJoueurNationality" class="form-label">Nationalité</label>
                        <input type="text" class="form-control" name="nationalite" id="editJoueurNationality" value="'.$joueurs[$i]->nationalite.'">
                      </div>
                      <div class="mb-3">
                        <input type="hidden" class="form-control" name="idJoueur" id="editJoueurID" value="'.$joueurs[$i]->idJoueur.'">
                      </div>
                      <button name="modifJoueur" type="submit" class="btn btn-primary">Enregistrer</button>
                    </form>
                    </div>
                    </div>
                </div>
                </div>';
            }
        ?>
      </ul>
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
        Utilisateurs
      </button>
    </h2>
    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#mainAccordion">
      <div class="accordion-body">
      <ul class="list-group">
        <li class="list-group-item" style="justify-content:center !important;"><button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#modalUserAjout">Ajouter un utilisateur <i data-feather="plus"></i></button></li>
            <!-- Modal -->
            <div class="modal fade" id="modalUserAjout" tabindex="-1" aria-labelledby="modalUserAjoutLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalUserAjoutLabel">Nouvel utilisateur</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                <form method="post" action="<?= $path ?>/index.php/add">
                  <div class="mb-3">
                    <label for="newUserUsername" class="form-label">Nom d'utilisateur</label>
                    <input type="text" class="form-control" name="username" id="newUserUsername">
                  </div>
                  <div class="input-group">
                    <span class="input-group-text">Nom et Prénom</span>
                    <input type="text" class="form-control" name="nom">
                    <input type="text" class="form-control" name="prenom">
                  </div>
                  <br>
                  <div class="input-group">
                    <span class="input-group-text">Adresse et CP</span>
                    <input type="text" class="form-control" name="adresse">
                    <input type="number" min="0" max="99999" class="form-control" name="cpVille">
                  </div>
                  <br>
                  <div class="mb-3">
                    <label for="newJoueurAge" class="form-label">Age</label>
                    <input type="number" class="form-control" name="age" id="newJoueurAge">
                  </div>
                  <div class="mb-3">
                    <label for="newUserAdmin" class="form-label">Rôle</label>
                    <select name="admin" id="newUserAdmin" class="form-select" aria-label="newUserAdmin">
                      <option value="1"selected>Administrateur</option>
                      <option value="0">Arbitre</option>
                    </select>
                  </div>
                  <div class="mb-3">
                    <label for="newUserPass" class="form-label">Mot de passe</label>
                    <input type="password" class="form-control" name="mdp" id="newUserPass">
                  </div>
                  <div class="mb-3">
                    <label for="newUserPass2" class="form-label">Répeter mot de passe</label>
                    <input type="password" class="form-control" name="mdp2" id="newUserPass2">
                  </div>
                  <button type="submit" class="btn btn-primary">Enregistrer</button>
                </form>
                </div>
                </div>
            </div>
            </div>
            <?php
            $dao_utilisateur = new DAO_Utilisateur();
            $utilisateurs = $dao_utilisateur->findAll();
            if ($utilisateurs == null) {
              echo 'Aucun joueur dans la BD';
            } else {
              for($i=0; $i<sizeof($utilisateurs); $i++) {
                echo '<li class="list-group-item"><span>'.$utilisateurs[$i]->prenom.' '.$utilisateurs[$i]->nom.'</span><span><button type="button" class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#modalDeleteUser'.$i.'"><i data-feather="trash"></i></button>&#8239;<button type="button" class="btn btn-sm btn-secondary" data-bs-toggle="modal" data-bs-target="#modalUser'.$i.'">Modifier <i data-feather="edit"></i></button></span></li>
                <!-- Modal -->
                <div class="modal fade" id="modalDeleteUser'.$i.'" tabindex="-1" aria-labelledby="modalDeleteUser'.$i.'Label" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="modalDeleteUser'.$i.'Label">Suppression</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div class="modal-body">
                        Êtes-vous sûr de vouloir supprimer définitivement ce joueur ?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                        <a href="'.$path.'/index.php/delete_user?id='.$utilisateurs[$i]->id.'"><button type="button" class="btn btn-danger">Supprimer</button></a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="modalUser'.$i.'" tabindex="-1" aria-labelledby="modalUser'.$i.'Label" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalUser'.$i.'Label">Modifier utilisateur</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    <form method="post" action="'. $path .'/index.php/modify">
                      <div class="mb-3">
                        <label for="editUserUsername" class="form-label">Nom d\'utilisateur</label>
                        <input type="text" class="form-control" name="username" id="editUserUsername" value="'.$utilisateurs[$i]->username.'">
                      </div>
                      <div class="input-group">
                        <span class="input-group-text">Nom et Prénom</span>
                        <input type="text" class="form-control" name="nom" value="'.$utilisateurs[$i]->nom.'">
                        <input type="text" class="form-control" name="prenom" value="'.$utilisateurs[$i]->prenom.'">
                      </div>
                      <br>
                      <div class="input-group">
                        <span class="input-group-text">Adresse et CP</span>
                        <input type="text" class="form-control" name="adresse" value="'.$utilisateurs[$i]->adresse.'">
                        <input type="number" min="0" max="99999" class="form-control" name="cpVille" value="'.$utilisateurs[$i]->cpVille.'">
                      </div>
                      <br>
                      <div class="mb-3">
                        <label for="editJoueurAge" class="form-label">Age</label>
                        <input type="number" class="form-control" name="age" id="editJoueurAge" value="'.$utilisateurs[$i]->age.'">
                      </div>
                      <div class="mb-3">
                        <label for="editUserAdmin" class="form-label">Rôle</label>
                        <select name="admin" id="newUserAdmin" class="form-select" aria-label="editUserAdmin" value="'.$utilisateurs[$i]->admin.'">
                          <option value="1" ';
                          if ($utilisateurs[$i]->admin==1) echo 'selected ';
                          echo
                          '>Administrateur</option>
                          <option value="0" ';
                          if ($utilisateurs[$i]->admin==0) echo 'selected ';
                          echo '>Arbitre</option>
                        </select>
                      </div>
                      <div class="mb-3">
                        <label for="editUserPass" class="form-label">Mot de passe</label>
                        <input type="password" class="form-control" name="mdp" id="editUserPass">
                      </div>
                      <div class="mb-3">
                        <label for="editUserPass2" class="form-label">Répeter mot de passe</label>
                        <input type="password" class="form-control" name="mdp2" id="editUserPass2">
                      </div>
                      <div class="mb-3">
                        <input type="hidden" class="form-control" name="id" id="editUserID" value="'.$utilisateurs[$i]->id.'">
                      </div>
                      <p class="text-muted">Ne pas remplir le mot de passe pour ne pas le modifier</p>
                      <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </form>
                    </div>
                    </div>
                </div>
                </div>';
              }
            }
          }
        ?>
      </ul>
      </div>
    </div>
  </div>
</div>
