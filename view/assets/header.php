<!DOCTYPE html>
<html>
<head>
    <title>ClaroTennis</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="<?= $GLOBALS['path'] ?>/view/assets/logo_clarotennis.png" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= $GLOBALS['path'] ?>/view/assets/style.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>    <script src="https://unpkg.com/feather-icons"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</head>

<body>
  <header class="p-3 bg-dark text-white">
    <div class="container">
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="<?= $GLOBALS['path'] ?>/index.php" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none me-lg-auto">
            <img src="<?= $GLOBALS['path'] ?>/view/assets/logo_clarotennis_grand.png" class="bi me-2" width="290" height="40" alt="Clarotennis Logo"/>
        </a>
        <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-2 me-lg-3" method="get" action="<?= $GLOBALS['path'] ?>/index.php/search">
          <input name="search_input" type="search" class="form-control form-control-dark" placeholder="Recherche..." aria-label="Search">
        </form>
        <div class="text-end">
          <?= $connection_button ?>
        </div>
      </div>
    </div>
  </header>
  <?php
    if(isset($_SESSION['info'])) {
      echo '<div class="toast" id="notification" role="alert" aria-live="assertive" aria-atomic="true" style="z-index: 1000; position: absolute; margin:10px;">
      <div class="toast-body">
        '.$_SESSION['info'].'
      </div>';
      unset($_SESSION['info']);
    }
  ?>
</div>
  <main>
