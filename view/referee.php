<h1>Interface Arbitre</h1>
<!-- Accordéon -->
<div class="accordion" id="mainAccordion">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
        Tournois
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#mainAccordion">
      <div class="accordion-body">
      <ul class="list-group">
        <?php
            $dao_tournoi = new DAO_Tournoi();
            $tournois = $dao_tournoi->findAll();
            if ($tournois == null) {
              echo 'Aucun Tournoi dans la BD';
            } else {
              for($i=0; $i<sizeof($tournois); $i++) {
                echo '<li class="list-group-item"><span>'.$tournois[$i]->nom.'</span><span>&#8239;<button type="button" class="btn btn-sm btn-secondary" data-bs-toggle="modal" data-bs-target="#modalSeeTournoi'.$i.'">Voir plus <i data-feather="arrow-right"></i></button></span></li>
                <!-- Modal -->
                <div class="modal fade" id="modalSeeTournoi'.$i.'" tabindex="-1" aria-labelledby="modalSeeTournoi'.$i.'Label" aria-hidden="true">
                <div class="modal-dialog" style="max-width: 1000px;">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalSeeTournoi'.$i.'Label">Tournoi</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    <h3>Tournoi</h3>';
                    $dao_matchs = new DAO_Matchs();
                    $dao_tour = new DAO_Tour();
                    $dao_joueur = new DAO_Joueur();
                    $tournament = generateTournament($tournois[$i]->idTournoi);
                    displayTournament($tournament);
                    $tours = $dao_tour->findByTournamentID($tournois[$i]->idTournoi);
                    $matchs = null;
                    foreach($tours as $tour) {
                      if(!empty($matchs)) {
                        $matchs = array_merge($matchs, $dao_matchs->findByRound($tour->idTour));
                      } else {
                        $matchs = $dao_matchs->findByRound($tour->idTour);
                      }
                    }

                    echo '<h3>Matchs</h3>
                    <div class="accordion" id="accordionTournoi'.$tournois[$i]->idTournoi.'">';
                    foreach($matchs as $match) {
                      $joueur1 = $dao_joueur->findByID($match->idJoueur1);
                      $joueur2 = $dao_joueur->findByID($match->idJoueur2);
                      $score1 = $match->score1;
                      $score2 = $match->score2;
                      if($match->idJoueur1 != -1 && $match->idJoueur2 != -1) {
                      echo '
                      <div class="accordion-item">
                        <h2 class="accordion-header" id="heading'.$match->idMatch.'">
                          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse'.$match->idMatch.'" aria-expanded="false" aria-controls="collapse'.$match->idMatch.'">
                          '.$joueur1->nom.' VS '.$joueur2->nom.'
                          </button>
                        </h2>
                        <div id="collapse'.$match->idMatch.'" class="accordion-collapse collapse" aria-labelledby="heading'.$match->idMatch.'" data-bs-parent="#accordionTournoi'.$tournois[$i]->idTournoi.'">
                          <div class="accordion-body">
                          <form method="post" action="'.$path.'/index.php/add_scores">
                              <input type="hidden" name="idMatch" value="'.$match->idMatch.'">
                              <div class="mb-3">
                                <label for="score1" class="form-label">Score '.$joueur1->nom.'</label>
                                <input type="integer" class="form-control" id="score1" name="score1" value="'.$score1.'">
                              </div>
                              <div class="mb-3">
                                <label for="score2" class="form-label">Score '.$joueur2->nom.'</label>
                                <input type="integer" class="form-control" id="score2" name="score2" value="'.$score2.'">
                              </div>
                              <button type="submit" class="btn btn-primary">Sauvegarder</button>
                            </form>
                          </div>
                        </div>
                      </div>
                      ';
                      }
                    }
                    echo '
                        </div>
                      </div>
                    </div>
                </div>
                </div>';
              }
            }
        ?>
      </ul>
      </div>
    </div>
  </div>
</div>