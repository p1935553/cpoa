<div class="form-signin">
  <form class="inner-form-signin" method="post" action="<?= $GLOBALS['path'] ?>/index.php/signin">
    <img class="mb-4" src="<?= $GLOBALS['path'] ?>/view/assets/logo_clarotennis.png" alt="" width="72" height="57">
    <h1 class="h3 mb-3 fw-normal">Connexion</h1>

    <div class="form-floating">
      <input type="text" class="form-control" id="floatingInput" name="username" placeholder="Nom d'utilisateur">
      <label for="floatingInput">Nom d'utilisateur</label>
    </div>
    <div class="form-floating">
      <input type="password" class="form-control" id="floatingPassword" name="password" placeholder="Mot de passe" autocomplete>
      <label for="floatingPassword">Mot de passe</label>
    </div>
    <button class="w-100 btn btn-lg btn-primary" name="submit" type="submit">Se connecter</button>
  </form>
</div>
