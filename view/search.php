<?php
if (!isset($_GET['search_input']) || empty($_GET['search_input'])){
  header('Location: ' . $path . '/index.php');
}
require_once('variables.php');
require_once('model/DAO_Tournoi.php');
require_once('model/DTO_Tournoi.php');
require_once('model/DAO_Joueur.php');
require_once('model/DTO_Joueur.php');
require_once('brackets.php');
?>

<!-- Accordéon -->
<h1>Résultats de la recherche</h1>
<div class="accordion" id="mainAccordion">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
        Tournois
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#mainAccordion">
      <div class="accordion-body">
      <ul class="list-group">
        <?php
        $dao_tournoi = new DAO_Tournoi();
        $tournois = $dao_tournoi->findLike(htmlspecialchars($_GET['search_input']));
        if ($tournois == null) {
          echo '<li class="list-group-item">Aucun résultat</li>';
        } else {
          foreach($tournois as $tournoi) {
            echo '<li class="list-group-item"><span>'.$tournoi->nom.'</span><button type="button" class="btn btn-sm btn-secondary" data-bs-toggle="modal" data-bs-target="#modalTournoi'.$tournoi->idTournoi.'">Voir plus <i data-feather="arrow-right"></i></button></li>
            <!-- Modal -->
            <div class="modal fade" id="modalTournoi'.$tournoi->idTournoi.'" tabindex="-1" aria-labelledby="modalTournoi'.$tournoi->idTournoi.'Label" aria-hidden="true">
            <div class="modal-dialog" style="max-width: 1000px;">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTournoi'.$tournoi->idTournoi.'Label">Détails tournoi</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">';
                $tournament = generateTournament($tournoi->idTournoi);
                displayTournament($tournament);
                echo '</div>
                </div>
            </div>
            </div>';
          }
        }
        ?>
      </ul>
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        Joueurs
      </button>
    </h2>
    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#mainAccordion">
      <div class="accordion-body">
      <ul class="list-group">
        <?php
            $dao_joueur = new DAO_Joueur();
            $joueurs = $dao_joueur->findLike(htmlspecialchars($_GET['search_input']));
            if ($joueurs == null) {
              echo '<li class="list-group-item">Aucun résultat</li>';
            } else {
              for($i=0; $i<sizeof($joueurs); $i++) {
                echo '<li class="list-group-item"><span>'. $joueurs[$i]->prenom .' '. $joueurs[$i]->nom .'</span><button type="button" class="btn btn-sm btn-secondary" data-bs-toggle="modal" data-bs-target="#modalPlayer'.$i.'">Voir plus <i data-feather="arrow-right"></i></button></li>
                <!-- Modal -->
                <div class="modal fade" id="modalPlayer'.$i.'" tabindex="-1" aria-labelledby="modalPlayer'.$i.'Label" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalPlayer'.$i.'Label">Joueur '. $joueurs[$i]->idJoueur .'</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <h3>'. $joueurs[$i]->prenom .' '. $joueurs[$i]->nom .'</h3>
                        <h5>Age : '. $joueurs[$i]->age .'</h5>
                        <h5>Nationalité : '. $joueurs[$i]->nationalite .'</h5>
                        <h5>Victoire(s) : '. $dao_joueur->countVictories($joueurs[$i]->idJoueur) .'</h5>
                    </div>
                    </div>
                </div>
                </div>';
            } 
          }
        ?>
      </ul>
      </div>
    </div>
  </div>
</div>