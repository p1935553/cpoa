<!-- basic styles -->
<style type="text/css">
canvas {  }
	.g_gracket { width: 100%; background-color: #f5f5f5; padding: 55px 15px 5px; line-height: 100%; position: relative; overflow: hidden;}
	.g_round { float: left; margin-right: 70px; }
	.g_game { position: relative; margin-bottom: 15px; color: #212529;}
	.g_game a { text-decoration: none; color: white;}
	.g_gracket h3 { margin: 0; padding: 10px 8px 8px; font-size: 14px; font-weight: normal; }
	.g_team { display: flex; justify-content: space-between; background: #212529; border:1px solid #212529; }
	.g_team:last-child { background: #9FBA3A; }
	.g_winner { background: #212529; border: 2px solid #9FBA3A; }
	.g_winner .g_team { background: none; }
	.g_current { cursor: pointer; background: #3139ad!important; }
	.g_round_label { top: 1px; font-weight: normal; color: #212529; text-align: center; font-size: 18px; }
</style>

<!-- main lib -->
<script type="text/javascript" src="/dependencies/jquery.gracket.min.js"></script>
<script type="text/javascript" src="/dependencies/test.js"></script>

<?php
	//Générateur de rounds
	function generateRound($joueur, $scores){
			$round = "[";
			for($i=0; $i<count($joueur); $i=$i+2){
				$round = $round . '[ { "name" : "' . $joueur[$i]->nom . '", "id" : "' . $joueur[$i]->idJoueur . '", "score" : "'.$scores[$i].'"},
				 { "name" : "' . $joueur[$i+1]->nom . '", "id" : "' . $joueur[$i+1]->idJoueur . '", "score" : "'.$scores[$i+1].'"} ],';
			}

			$round= $round . "],";

		return $round;
	}

	//Générateur de tournois
	function generateTournament($idTournament){
		$tournament = "[";
		$i=1;
		$dao_joueur = new DAO_Joueur();
		$dao_tour = new DAO_Tour();
		$dao_matchs = new DAO_Matchs();
		$dao_tournoi = new DAO_Tournoi();
		$tournoi = $dao_tournoi->findByID($idTournament);
		$gagnant = $dao_joueur->findByID($tournoi->idGagnant);
		$tours = $dao_tour->findByTournamentID($idTournament);
		foreach($tours as $tour) {
			if(!empty($matchs)) {
			$matchs = array_merge($matchs, $dao_matchs->findByRound($tour->idTour));
			} else {
			$matchs = $dao_matchs->findByRound($tour->idTour);
			}
		}
		foreach($matchs as $match) {
			$scores[] = $match->score1;
			$scores[] = $match->score2;
		}
		$joueurs = $dao_joueur->findByTournamentAndRoundId($idTournament, $i);
		$nbJoueurs = count($joueurs);
		while($joueurs){
			$i++;
			$tournament = $tournament . generateRound($joueurs, $scores);
			array_splice($scores, 0, count($joueurs));
			$joueurs = $dao_joueur->findByTournamentAndRoundId($idTournament, $i);
			if(count($joueurs)!=1){
				$nbJoueurs = count($joueurs);
			}else{
				$nbJoueurs = $nbJoueurs /2;
				$joueurs = null;
				$joueur_null = $dao_joueur->findByID(-1);
				for($j=0; $j<$nbJoueurs; $j++){
					$joueurs[] = $joueur_null;
				}
			}

		}

		$tournament = $tournament . '[[{ "name" : "' . $gagnant->nom . '", "id" : "' .$gagnant->idJoueur.'" }]]]';
		return $tournament;
	}

	function displayTournament($tournament){
		echo '<!-- empty gracket element -->
		<div class="my_gracket"></div>

		<script type="text/javascript">
			(function(win, doc, $){


				// Fake Data
				win.Data = ' . $tournament . '

				// initializer
				$(".my_gracket").gracket({ src : win.Data });

			})(window, document, jQuery);
		</script>';
	}
?>
